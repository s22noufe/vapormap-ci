## workflow de la chaine

- les stages s'exécutent de façon séquentielle et les jobs dans les mêmes stages s'exécutent en parallèle
- Dans le stage "test" le job 'flake8-test' qui fait le test du code de l'API s'exécute d'abord ayant pas mis de condition dessus, ensuite le job 'test_dockerfile_api' attend 2 secondes après l’exécution du job de test de l'API supposant que le test est bon et le code de l'API est de bonne qualité le job du test du Dockerfile de l'API s'exécute pour fournir également des résultats sur le Dockerfile de l'API. Le job 'bootlint-test' attend 4 secondes pour s'exécuter après que le job du test de l'API et du dockerfile de l'API soit effectué, après exécution du job de test du code du frontend le job de test 'test_dockerfile_front' s'exécute par la suite ayant attendu 6 secondes que le test du code frontend soit bon.
- Dans le stage "deploy" le job 'pages' s'exécute sans condition dès que les jobs de test ont fini de s'exécuter et générer les différends résultats de tests et artefacts car ce job à une relation avec les différends jobs de test
- Dans le stage "build" les jobs 'buid_api' et 'build_frontend' sont pris en compte dans le pipeline lorsque la condition établie sur eux est respectée qui est d'être prisent en compte lorsqu'un commit est fait dans la branche main où on travail et que la modification concerne le fichier 'Dockerfile'. Quant au job 'mariadb' ayant pas de condition, il s'exécute lorsque le pipeline est en marche
- Dans le stage "deploy" le job 'deploy' est pris en compte dans le pipeline lorsqu'une la condition établie sur ce job est respectée qui est d'être pris en compte lorsqu'un commit est fait dans la branche main où on travail et que la modification concerne le répertoire 'API_PROD' et 'FRONTEND_PROD' qui sont les deux répertoires de l'application à déployer.

## Comment utiliser la chaine
- se rendre sur Gitlab où est versionné le projet grâce à l'URL du projet : https://gitlab.imt-atlantique.fr/s22noufe/vapormap-ci
- se rendre dans CI/CD >> Editor pour éditer le pipeline et lancer un changement de commit pour déclencher le pipeline
- si le projet a été cloné en local, on peut faire un 'commit' en étant dans la branche main du projet le pipeline va se déclencher également
- Une fois le pipeline déclencher, se rendre dans CI/CD >> pipeline pour visualiser l'exécution du pipeline en se référant au workflow de la chaine
- on peut afficher les logs de chaque job en cliquant sur le job après son exécution et voir les dépendances entre les jobs également
- lorsque les jobs qui génèrent ou utilisent des artefacts générés par d'autre job s'exécutent on peut voir dans l'interface graphique ces artefacts à côté des logs du job en question exécuté
- les résultats de test sont publiés dans settings >> pages, il suffit de cliquer sur le lien de la page pour voir les résultats de test sous forme de page web