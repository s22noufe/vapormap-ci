Nom: NOUFE Sié Schadrac  
Date: 10/01/2023
Classe: MS-ICD
Contexte: Projet Fil Rouge Chaîne d'intégration continue avec Gitlab-CI
Nom du projet sur Gitlab : VAPORMAP-CI 
URL du projet sur Gitlab : https://gitlab.imt-atlantique.fr/s22noufe/vapormap-ci

# Objectif du projet
Le projet a pour objectif de faire les tests et intégration continue de l'application vapormap
# Présentation de ce qui a été fait

## Préparation de la chaine d'intégration continue dans Gitlab-CI
- Création d'un dépôt dédié à la chaine d'intégration continue nommer "VAPORMAP-CI"
- Création du fichier de configuration de la chaine CI "gitlab-ci.yml" 
- versionnage des fichiers de l'applications vapormap, des Dockerfile, du fichier docker-compose.yml et du fichier de configuration de la chaine CI vers ce nouveau dépôt "VAPORMAP-CI" afin de m'en servir dans la chaine CI à construire
## Configuration de base de la chaine d'intégration
- Pour la conception du pipeline, une configuration de base a été faite consistant à définir d'abord les variables en début du pipeline comme variable globale ou prédéfinis qui sont transmises et copiées dans la configuration de certains jobs 
- Création des stages "test" (étape d'intégration), "build" (étape du Delivery) et "deploy" (étape du Deploy) qui sont des étapes dans lesquelles des jobs vont s'exécuter.Les jobs qui sont dans le même stage s'exécutent en parallèle.
## Étape 1 : Intégration
Cette partie consiste à créer des tâches qui vont réaliser les tests sur notre application "vapormap" à déployer afin de savoir si le code de notre application est de qualité ou pas, si certaines fonctions de l'applications sont respectées pour livrer l'application et la déployée
### Réaliser des tests sur les applications API et frontend
* test API
- Création d'une tâche "flake8-test" et utilisation du linter flake8(linter de test python) pour tester le code de l'API qui a été développé avec flask qui est un framework de python avec des seuils recommandés dans les bonnes pratiques
- Ce test fournit des résultats sur le code de l'API en vérifiant le style du code, la syntaxe du code et la complexité du code
- le résultat du test est dirigé dans un fichier "api.html"
- un artefact est généré par ce job en spécifiant le répertoire contenant le fichier "api.html" à enregistrer en tant qu’artefact de travail et uploadé sur le serveur Gitlab à la fin de ce job puis téléchargé depuis le serveur Gitlab au début du job qui l'utilise
- Utilisation d'une condition "|| true" afin de rendre ce job non bloquant 
- Ce job a été exécuté dans le stage "test" car il s'agit du test, avec une image python et a été confié au Runner Docker avec le tag 'ms-icd'.

* test FRONTEND
- Création d'une tâche "bootlint-test" et utilisation du linter bootlint pour tester le code du frontend où se trouve les fichiers statiques(javascript,html,boostrap) avec des seuils recommandés dans les bonnes pratiques
- Ce test fournit des résultats sur le code du frontend en vérifiant la présence de plusieurs erreurs HTML courantes dans la page Web qui utilise Bootstrap, en vérifiant que les instances des composants Bootstrap ont un HTML correctement structuré
- le résultat du test est dirigé dans un fichier "front.html"
- un artefact est généré par ce job en spécifiant le répertoire contenant le fichier "front.html" à enregistrer en tant qu’artefact de travail et uploadé sur le serveur Gitlab à la fin de ce job puis téléchargé depuis le serveur Gitlab au début du job qui l'utilise
- Utilisation d'une condition "|| true" afin de rendre ce job non bloquant et d'une condition "sleep" avec un temps indiqué afin d'attendre l'exécution de certains jobs avant de s'exécuter
- Ce job a été exécuté dans le stage "test" car il s'agit du test, avec une image node et a été confié au Runner Docker avec le tag 'ms-icd'.

* tests Dockerfile API et FRONTEND
- Création de deux tâches "test_dockerfile_api" et "test_dockerfile_front" puis utilisation du linter hadolint dans chaque tâche pour tester le Dockerfile de l'api et le Dockerfile du frontend avec des seuils recommandés dans les bonnes pratiquespour dans le but de m'assurer que je crée des images petites, sécurisées, efficaces et faciles à maintenir 
- Ces tests fournissent des résultats sur les deux Dockerfiles en repérant les problèmes courants, un arbre syntaxique abstrait (AST) est utilisé pour analyser les deux Dockerfile en fonction de règles prédéfinies, ShellCheck est integré ce qui permet de vérifier les scripts shell dans les instructions RUN des deux Dockerfile.
- le résultat des tests est dirigé dans un fichier "docker_api.html" et "docker_front.html"
- un artefact est généré par chaque job en spécifiant le répertoire contenant le fichier "docker_api.html" et "docker_front.html" à enregistrer en tant qu’artefact de travail dans chacun des deux jobs et uploadé sur le serveur Gitlab à la fin de chaque job puis téléchargé depuis le serveur Gitlab au début du job qui l'utilise
- Utilisation d'une condition "|| true" afin de rendre ces jobs non bloquants et d'une condition "sleep" avec un temps indiqué afin d'attendre l'exécution de certains jobs avant de s'exécute
- Ces job ont été exécuté dans le stage "test" car il s'agit des tests, avec une image docker.io/hadolint et ont été confié au Runner Docker avec le tag 'ms-icd'.

* publication des résultats de test dans "Gitlab Pages"
- Création d'une tâche "pages" pour publier les différends résultats de tests effectués. Cette tâche sera chargée de télécharger du contenu statique sur GitLab et de publier ce contenu sous forme de site Web. 
- Création d'une relation entre ce job "pages" et les différends jobs de test car ce job est dédié pour la publication des résultats des jobs de test dans Gitlab Pages afin d'afficher ces résultats sous forme de site web.
- Affichage des différends fichiers contenant les résultats de test après avoir téléchargé les différends artifcats générés, redirection de ces différends fichiers de tests vers un fichier public/index.htm qu'on retrouvera dans Gitalab Pages dans un lien à cliquer pour déployer la page
- un artefact public est généré par ce job en spécifiant le répertoire "public" à enregistrer en tant qu’artefact de travail et uploadé sur le serveur Gitlab à la fin de ce job puis téléchargé depuis le serveur Gitlab par des utilisateurs anonymes et invités
- Utilisation d'une condition "when: always" dans l'artefact pour toujours télécharger des artefacts étant dans ce job (sauf lorsque les travaux expirent à travers des conditions établies sur la durée) ce qui permettrait de toujours publier les résultats de tests grâce aux artefacts des jobs de tests générés.
- Ce job a été exécuté dans le stage "deploy" vue que c'est une tâche qui consiste à déployer des résultats de tests, avec une image node et a été confié au Runner Docker avec le tag 'ms-icd'.

### Livrables

 * fichier gitlab-ci.yml : voir sur le dépôt Gitlab du projet dont l'URL se trouve au-dessus  du rapport
 * copie d'écran du workflow d'intégration : voir annexe (capture workflow intégration)
 * Résultat des tests publiés dans l'espace "Gitlab Pages" du projet : voir sur le dépôt Gitlab du projet dans settings >> pages dont l'URL se trouve au-dessus du rapport

## Étape 2 : Delivery
Cette partie consiste à créer des tâches qui vont réaliser la livraison de notre application "vapormap" en construisant des images qu'on va publier dans la registry Gitlab du projet, et utiliser ces images pour tester manuellement notre application dans le terminal
### Build des images 
* job non exécuté 
- Création d'une tâche non exécute ".docker_build" sur lequel des jobs vont s'appuyer et permettant de factoriser du code
- Utilisation du service docker-in-docker (dind) pour spécifier les images Docker supplémentaires dont nos scripts ont besoin pour s’exécuter correctement
- Réalisation d'une instruction affichant des informations docker dans la partie before script suivie des instructions de la tâche qui affichent les valeurs des variables de CI liées à la registry
- Ce job a été exécuté dans le stage "build", avec une image docker et a été confié au Runner Docker avec le tag 'ms-icd'.

* job de construction de l'image de l'API et FRONTEND
- Création de deux tâches "build_API" et "build_FRONTEND" qui s'appuient sur le job ".docker_build" grâce à la condition extends afin de réutiliser les sections de configuration du job ".docker_build" pour s'exécuter
- Connexion à la registry Gitlab du projet en passant les variables CI liées à la registry docker à l'utilisateur (-u) et le mot de passe (-p)
- Construction de l'image dans les répertoires API_PROD et FRONTEND_PROD du projet avec un tag défini comme variable dans la partie variable au debut de la chaine
- push des images construites vers la registry docker du projet
- Définition d'une condition avec rules permettant d’inclure ou d’exclure ces jobs dans l'exécution du pipeline. Si un commit s'effectue sur une branche qui est égale à 'main' où se déroule le projet, vérifie les modifications liées aux fichiers "Dockerfile" et S'ils ont changé, ajoute les tâches au pipeline pour que la reconstruction et publication vers la registry des images se fasse en prenant en compte les nouvelles modifications cas contraire n'ajoute pas les tâches au pipeline et le pipeline continue de s’exécuter même si les tâches ne sont pas déclenchées
- Ces jobs ont été exécutés dans le stage "build", avec une image docker et ont été confié au Runner Docker avec le tag 'ms-icd'.

* job de construction de l'image de la base de donnée
- Création d'une tâche "mariadb" qui s'appuie sur le job ".docker_build" grâc à la condition extends afin de réutiliser les sections de configuration du job ".docker_build" pour s'exécuter
- Connexion à la registry Gitlab du projet en passant les variables CI liées à la registry docker à l'utilisateur (-u) et le mot de passe (-p)
- Récupération de l'image mariadb (pull) avec un tag depuis le dockerhub afin de l'utiliser dans notre environnement
- Changement du tag de l'image mariadb en un nouveau tag défini comme variable dans la partie variable au depuis de la chaine CI
- push de l'image construite vers la registry docker du projet afin de sécuriser le déploiement 
- Ce job a été exécuté dans le stage "build", avec une image docker et a été confié au Runner Docker avec le tag 'ms-icd'.

* test manuel de l'application hors chaine CI
- connexion à la registry Gitlab du projet avec un token crée afin d'accéder aux différends images stockées dans la registry Gitlab du projet depuis le terminal
- modification du fichier docker-compose.yml en mettant devant chaque image des différends services du stack les liens des différends images publiées dans la registry Gitlab du projet 
- lancement d'une commande docker pour démarrer la composition, le pull des différends images stockées dans la registry Gitlab du projet est effectué suivie de la construction des différnds services du stack
- utilisation des ports publiés si sommes en local pour accéder aux différends conteneurs qui hébergent les services de l'application

### Livrables

* Les images publiées dans la registry Docker du projet : voir sur le dépôt Gitlab du projet dans packages and registries >> container registry dont l'URL se trouve au-dessus du rapport
* Un fichier "docker-compose.yaml" permettant de valider le bon fonctionnement de l'application à partir de ces images: voir sur le dépôt Gitlab du projet dont l'URL se trouve au-dessus du rapport
* test manuel hors de la CI : voir annexe (capture du test manuel de l'application hors de la CI)

## Étape 3 : Deploy
Cette partie consiste à créer une tâche qui va réaliser le déploiement de notre application "vapormap" en utilisant un runner de déploiement qui réalisera l'exécution de la commande docker compose up dans un environnement Docker (VM VDI)

### Déploiement de l'application
* Installation d'un runner shell sur ma machine VDI et enregistrement du runner sur le Gitlab du projet
- Ajout du dépôt officiel GitLab grâce à une commande
- Installation de la dernière version de GitLab Runner
- Enregistrement du runner en entrant l'instance URL de notre Gitlab où se situe notre projet, le token obtenu pour enregistrer le runner, la description du runner, le tag associé au runner, fournir l'exécuteur du runner qui est shell
- Vérification dans la liste des runner dans l'interface de Gitlab du projet que le runner a été bien enregistré

* déploiement automatique de l'application
- Création d'une tâche "deploy" dans le stage 'deploy' qui permettra de déployer l'application 
- Sélection du runner shell qu'on vient de créer grâce à son tag pour exécuter ce job 
- ajout du groupe docker au runner shell crée afin de pouvoir exécuter une commande docker dessus
- connexion à la registry Gitlab du projet avec un token crée afin d'accéder aux différends images stockées dans la registry Gitlab du projet depuis le shell
- lancement d'une commande docker pour démarrer la composition en mode deamon, le pull des différends images stockées dans la registry Gitlab du projet est effectué suivie de la construction des différends services du stack
- utilisation des ports publiés si sommes en local pour accéder aux différends conteneurs qui hébergent les services de l'application
- Définition d'une condition avec rules permettant d’inclure ou d’exclure ce job dans l'exécution du pipeline. Si un commit s'effectue sur une branche qui est égale à 'main' où se déroule le projet, vérifie les modifications liées aux répertoires "API_PROD et FRONTEND_PROD" et S'ils ont changé, ajoute la tâche au pipeline pour qu'on déploie l'application en prenant en compte les nouvelles modifications cas contraire n'ajoute pas la tâche au pipeline et le pipeline continue de s’exécuter même si la tâche n'est pas déclenchée

### Livrables

* Description de ce qui a été mis en place pour déployer le runner: voir dans le rapport Étape 3 : Deploy >> Déploiement de l'application >> Installation d'un runner shell sur ma machine VDI et enregistrement du runner sur le Gitlab du projet
* Description de ce qui a été mis en place pour pouvoir déployer automatiquement l'application : voir dans le rapport Étape 3 : Deploy >> Déploiement de l'application >> déploiement automatique de l'application

## workflow
- les stages s'exécutent de façon séquentielle et les jobs dans les mêmes stages s'exécutent en parallèle
- Dans le stage "test" le job 'flake8-test' qui fait le test du code de l'API s'exécute d'abord ayant pas mis de condition dessus, ensuite le job 'test_dockerfile_api' attend 2 secondes après l’exécution du job de test de l'API supposant que le test est bon et le code de l'API est de bonne qualité le job du test du Dockerfile de l'API s'exécute pour fournir également des résultats sur le Dockerfile de l'API. Le job 'bootlint-test' attend 4 secondes pour s'exécuter après que le job du test de l'API et du dockerfile de l'API soit effectué, après exécution du job de test du code du frontend le job de test 'test_dockerfile_front' s'exécute par la suite ayant attendu 6 secondes que le test du code frontend soit bon.
- Dans le stage "deploy" le job 'pages' s'exécute sans condition dès que les jobs de test ont fini de s'exécuter et générer les différends résultats de tests et artefacts car ce job à une relation avec les différends jobs de test
- Dans le stage "build" les jobs 'buid_api' et 'build_frontend' sont pris en compte dans le pipeline lorsque la condition établie sur eux est respectée qui est d'être prisent en compte lorsqu'un commit est fait dans la branche main où on travail et que la modification concerne le fichier 'Dockerfile'. Quant au job 'mariadb' ayant pas de condition, il s'exécute lorsque le pipeline est en marche
- Dans le stage "deploy" le job 'deploy' est pris en compte dans le pipeline lorsqu'une la condition établie sur ce job est respectée qui est d'être pris en compte lorsqu'un commit est fait dans la branche main où on travail et que la modification concerne le répertoire 'API_PROD' et 'FRONTEND_PROD' qui sont les deux répertoires de l'application à déployer.


# Vécu du projet

## Difficultés rencontrées
* Mes différends tests étaient bons, mais les différends jobs de tests se bloquaient après avoir dirigé les différends résultats de tests dans les fichiers respectifs. La solution que j'ai trouvé était de mettre une condition || true ce qui rendait plus les jobs de test bloquant
* j'arrivais pas à exécuter une commande docker dans le runner shell après l'avoir crée, j'ai fait des recherches en m'appuyant sur l'erreur générée et la solution que j'ai trouvé était d'ajouter le groupe docker au runner shell crée afin d'exécuter des commandes docker dessus
* après avoir mis des conditions sur certains jobs, le pipeline se bloquait lorsque ces jobs avec des conditions étaient pas pris en compte , j'ai trouvé comme solution la condition allow_failure: true afin que le pipeline continue de s’exécuter même si la tâche n'est pas déclenchée
* lorsque je mettais une condition sur un job et qu'un autre job dépend de ce job conditionné, quand le job conditionné est pas pris en compte dans l'exécution du pipeline le job qui dépend de lui se bloque, ce qui est logique c'est après j'ai su et revu mes conditions

## Apprentissages
- Ce projet m'a permis de connaitre le but des tests et intégration continue d'une application, de savoir comment proccéder pour faire les tests et intégrer une application en continue
- j'ai pu crée des étapes(stage) sur lesquelles j'ai crée des tâches (job), j'ai confié ces jobs à des runners de projet ou des runners que j'ai personnellement crée pour exécuter ces jobs. Ces jobs ont pu s'échanger des données grâce à des artefacts que j'ai également crée et j'ai fini par définir des conditions d'exécution de certains jobs qui seront pris en compte dans la chaine lorsque ces conditions seront respectées
- Ce projet m'a permis de revoir la documentation Gitlab, le cours sur Gitlab qu'on a fait, de faire face à des problème comme les erreurs afin de chercher à les comprendre et trouver des solutions pour les résoudre.
- Grace à ce projet de Tests et Intégration continue de l'application "vapormap" sur Gitlab CI, j'ai pu comprendre et réaliser un autre projet de test et intégration continue avec le serveur d'intégration Jenkins.

## synthèse
A l'issue de ce projet nous voyons clairement le gain apporté par les tests et intégration continue d'une application. Lorsqu'on a une application, il faudrait tester le code de l'applications à travers des linter dans des jobs afin de s'assurer que le code est de qualité, les fonctions de l'applications à tester sont bons dans le but de prendre une décision pour livrer l'application ou revoir le code avant de livrer l'application pour qu'elle soit déployée à la fin. Ces différends tâches permettant de test et intégrer en continu l'application se fait dans un pipeline bien configuré et qui prend en compte chaque modification de l'application et intègre à nouveau l'application de façon automatique jusqu'au déploiement. Les pipelines jouent un rôle important dans l'automatisation des services.

# Annexe

##  Chaine CI/CD sans conditions définies
![capture de la CI sans les conditions](capture%20de%20la%20CI%20sans%20les%20conditions.png)

##  Chaine CI/CD avec conditions définies
![capture de la CI avec les conditions](capture%20de%20la%20CI%20avec%20les%20conditions.png)

## Livrables Étape 1 (Intégration) : workflow intégration
![capture workflow intégration](capture%20workflow%20int%C3%A9gration.png)

## Livrables Étape 2 (Delivery) : test manuel de l'application( hors de la CI)
![capture du test manuel de l'application( hors de la CI)](capture%20du%20test%20manuel%20.png)

## Livrables Étape 3 (Deploy) : job deploy
![capture du job deploy](capture%20du%20job%20deploy.png)


